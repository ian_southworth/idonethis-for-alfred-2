# iDoneThis For Alfred 2

## Contact
Script & attempt at creating an icon by:  
Chad Stovern  
<chadhs@digitalnomad.im>  
<http://www.digitalnomad.im>

## Summary

This workflow uses Mac OS X built–in sendmail binary to quickly send updates to your [iDoneThis.com](http://idonethis.com) account.

## Installation

Simply download and double–click the workflow file to install.  
Then edit the bash script to fill out your name and the email address tied to your [iDoneThis.com](http://idonethis.com) account; it will be at the top of the script and look just like this.  If you are using iDoneThis with a team you'll want to change the "to address" setting as well.

	## Set your name & email address here
	from_name="John Doe"
	from_addr="jdoe@example.com"

	## For use with a team change the to email address
	to_addr="today@idonethis.com"
	#to_addr="team-short-name@team.idonethis.com"

Type "idid" followed bey your update and press enter; you're done!

## Troubleshooting

If you have problems getting the plugin to work, try the following **[fix](http://www.alfredforum.com/topic/2747-idonethis-for-alfred-2/?p=16087)**.
